import s from "./toogleSteps.module.scss";
import cn from "classnames";

interface IToogleStepsProps{
    isGameOver: boolean;
    isX: boolean;
}

export const ToogleSteps: React.FC<IToogleStepsProps> = ({isGameOver, isX}) => {
    
  return (
    <div className={cn(s.toogleSteps, isGameOver && s.gameOver)}>
      <span>X</span>
      {!isGameOver && (
        <div className={s.line}>
          <div className={cn(s.circle, !isX && s.right)}></div>
        </div>
      )}
      <span className={s.zero}>O</span>
    </div>
  );
};
