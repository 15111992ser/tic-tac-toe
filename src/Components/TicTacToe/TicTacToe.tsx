import { useState } from "react";
import s from "./TicTacToe.module.scss";
import { Square } from "./Square/Square";
import { calculateWinner } from "../../utils/calculateWinner";
import { ToogleSteps } from "./ToogleSteps/ToogleSteps";

export const TicTacToe = () => {
  const [squares, setSquares] = useState(Array(9).fill(null));
  const [isX, setIsX] = useState(true);
  const [isGameOver, setIsGameOver] = useState<boolean>(false);

  if(!squares.includes(null)){
    setIsGameOver(true)
    setSquares(Array(9).fill(null))
  }

  const handleClickSquare = (i: number) => {
    squares[i] = isX ? "X" : "O";

    setSquares(squares);
    setIsX(!isX);
    setIsGameOver(!!calculateWinner(squares));
  };

  const handleRestart = () => {
    setIsGameOver(false);
    setIsX(true);
    setSquares(Array(9).fill(null));
  };

  return (
    <div className={s.wrapper}>
      {isGameOver  && (
        <div className={s.gameOverBlock}>
          <div className={s.wrapper}>
            {calculateWinner(squares) ? <p>{`Player ${calculateWinner(squares)} Win`}</p> : <p>Draw</p>}
            <button onClick={handleRestart}>Play again</button>
          </div>
        </div>
      )}
      <div className={s.mainContent}>
        <ToogleSteps isGameOver={isGameOver} isX={isX}/>
        <div className={s.board}>
          {squares.map((el, i) => (
            <Square key={i} value={squares[i]} onClick={() => handleClickSquare(i)} isGameOver={isGameOver} />
          ))}
        </div>
      </div>
    </div>
  );
};
