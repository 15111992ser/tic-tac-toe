import s from "./Square.module.scss";
import cn from "classnames";;

interface ISquareProps {
  value: string;
  onClick: ()=> void;
  isGameOver: boolean 
}

export const Square: React.FC<ISquareProps> = ({ value, onClick, isGameOver }) => {
  
  return (
      <button disabled={!!value} onClick={onClick} className={cn(s.item, isGameOver && s.gameOver)}>
        <span>{value}</span>
      </button>

  );
};
